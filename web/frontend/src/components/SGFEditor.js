import React, {useState} from "react";
import {useEffect} from "react";
import "../besogo/css/besogo.css"
import "../besogo/css/board-bold.css"
import besogo_func from "../besogo";
import "../styles/SGFEditor.css"
import HeaderPlayer from "./HeaderPlayer";
import axios from "axios";
import {backend} from "../config";
import {useParams} from "react-router-dom";

const black_errors = [{move: 1, type: "blunder"},
                      {move: 5, type: "mistake"},
                      {move: 11, type: "mistake"},
                      {move: 35, type: "blunder"},
                      {move: 47, type: "blunder"},]
const white_errors = [{move: 4, type: "blunder"},
                      {move: 10, type: "mistake"},
                      {move: 16, type: "mistake"},
                      {move: 30, type: "blunder"},
                      {move: 32, type: "blunder"},]

const TAGS_SEP = "===TAGS==="

function nextError(besogo, errors) {
    errors = errors.map(error=>error.move);
    let node = besogo.editor.getCurrent()
    if (node.children.length === 0) return
    node = node.children[0]
    while (node.children.length > 0 && errors.indexOf(node.moveNumber) === -1) {
        node = node.children[0]
    }
    besogo.editor.setCurrent(node);
}

function prevError(besogo, errors) {
    errors = errors.map(error=>error.move)
    let node = besogo.editor.getCurrent()
    if (node.parent === null) return
    node = node.parent
    while (node.parent !== null && errors.indexOf(node.moveNumber) === -1) {
        node = node.parent
    }
    besogo.editor.setCurrent(node);
}

function goToMove(besogo, move) {
    console.log(move)
    let node = besogo.editor.getCurrent()
    if (node.moveNumber === move) return;
    if (node.moveNumber < move) {
        besogo.editor.nextNode(move - node.moveNumber)
    } else {
        besogo.editor.prevNode(node.moveNumber - move)
    }
}

function SGFEditor({username}) {
    let besogo = besogo_func()

    const {game_id} = useParams()
    const [sgf, changeSgf] = useState("");
    const [tags, changeTags] = useState(Object.freeze([]))
    const [observedPlayer, changeObservedPlayer] = useState(0) // 0 - black, 1 - white
    const [black_errors, change_black_errors] = useState([])
    const [white_errors, change_white_errors] = useState([])
    const [analyzed, changeAnalyzed] = useState(false);

    const [errors, changeErrors] = useState(black_errors)

    useEffect(() => {
        if (observedPlayer === 0) changeErrors(black_errors)
        else changeErrors(white_errors)
    }, [observedPlayer, black_errors, white_errors])

    useEffect(() => {
        axios.get(
            backend + "api/player/get_game/",
            {
                params: {game_id: game_id},
                "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
            },
        ).then(response => {
            changeSgf(response.data.sgf)
            besogo = besogo_func()
            besogo.autoInit()
            besogo.editor.addListener((msg) => changeTags(ParseTags(besogo.editor.getCurrent().comment)))
            besogo.loadSgf(besogo.parseSgf(sgf), besogo.editor)

            return axios.get(
                backend + "api/player/get_mistakes/",
                {
                    params: {game_id: game_id},
                    "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
                }
            )
        }).then(response => {
            if (response.status === 204) {
                return;
            }
            changeAnalyzed(true);
            change_black_errors(response.data['Black'])
            change_white_errors(response.data['White'])
        }).catch(error => {
            console.log(error)
        })
    }, [sgf])
    const saveSGF = function () {
        axios.post(
            backend + "api/player/save_game/",
            {
                sgf: besogo.composeSgf(besogo.editor),
                game_id: game_id,
            },
            {
                "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
            }
        ).then(response => {
            alert('Сохранено!')
        }).catch(error => {
            alert('Ошибка')
        })
    }

    const downloadSGF = function () {
        saveSGF()
        var link = document.createElement('a'),
                blob = new Blob([besogo.composeSgf(besogo.editor)],
                    {encoding: "UTF-8", type: "text/plain;charset=UTF-8"});

            link.download = 'download.sgf'; // Set download file name
            link.href = URL.createObjectURL(blob);
            link.style.display = 'none'; // Make link hidden
            link.click(); // Click on link to initiate download
    }

    function addTag(old, tag) {
        if (!old) {
            return TAGS_SEP + "\n" + tag
        }
        if (old.indexOf(TAGS_SEP) === -1) {
            return old + "\n" + TAGS_SEP + "\n" + tag
        }
        return old + "\n" + tag
    }

    function deleteTag(old, tag) {
        const [main_comment, tags] = ParseComment(old)
        const new_tags = tags.filter(item => {return item !== tag})
        const new_com = main_comment + TAGS_SEP + "\n" + new_tags.join("\n")
        return new_com
    }

    function TagToAdd({name}) {
        return <div className={'tag'} onClick={() => {
            const new_comment = addTag(besogo.editor.getCurrent().comment, name)
            besogo.editor.setComment(new_comment)
            besogo.editor.notifyListeners({comment: new_comment})
        }
        }>
            {name}</div>
    }

    function CurrentTag({name}) {
        return <div className={'tag'} onClick={() => {
            const new_comment = deleteTag(besogo.editor.getCurrent().comment, name)
            besogo.editor.setComment(new_comment)
            besogo.editor.notifyListeners({comment: new_comment})
        }
        }>
            {name}</div>
    }

    return (<div>
        <HeaderPlayer username={username}/>
        <div className={"sgf"}>
        <div className={"besogo-editor"} resize="fixed"/>
        <div className={"left-part"}>
            <div className={"button"} onClick={saveSGF}>Сохранить</div>
            <div className={"button"} onClick={downloadSGF}>Сохранить и скачать</div>
            <div className={"toggle-player button"}
            onClick={e=>changeObservedPlayer(1-observedPlayer)}>{"Переключить на "+(observedPlayer?"черных":"белых")}</div>
            <div className={"mini-title"}>Ошибки:</div>
            <div className={"errors"}>
                {(analyzed) ?
                    errors.map(error => <div className={"error " + error.type} title={error.type}
                onClick={e=>{goToMove(besogo, error.move)}}>{error.move}</div>) :
                    <div>Игра обрабатывается...</div>}
            </div>
            <div className={"arrows"}>
                <div className={"arrow button"}
                     onClick={(e)=>{prevError(besogo, errors)}}>{"<"}Предыдущая ошибка</div>
                <div className={"arrow button"}
                     onClick={(e)=>{nextError(besogo, errors)}}>Следующая ошибка{">"}</div>
            </div>
            <div className={"mini-title"}>Теги:</div>
            <div className={"current-tags"}>
                {tags.map(item=><CurrentTag name={item}/>)}
            </div>
            <div className={"mini-title"}>Добавить тег:</div>
            <div className={"add-tags"}>
            <div className={"tags-container game-stage"}>
                <div className={"mini-title"}>Стадия игры:</div>
                <TagToAdd name={'Фусеки'}/>
                <TagToAdd name={'Джёсеки'}/>
                <TagToAdd name={'Тюбан'}/>
                <TagToAdd name={'Ёсэ'}/>
                <TagToAdd name={'Оценка баланса'}/>
                <TagToAdd name={'Ко-борьба'}/>
                <TagToAdd name={'Жизнь и смерть'}/>
                <TagToAdd name={'Техника сэмэай'}/>
            </div>
            <div className={"tags-container strategy"}>
                <div className={"mini-title"}>Стратегия:</div>
                <TagToAdd name={'Выбор направления игры'}/>
                <TagToAdd name={'Большой/срочный ход'}/>
                <TagToAdd name={'Соединение/разрезание групп'}/>
                <TagToAdd name={'Точка разрезания'}/>
                <TagToAdd name={'Баланс территории/влияния'}/>
                <TagToAdd name={'Использование силы'}/>
                <TagToAdd name={'Сентэ-готэ'}/>
                <TagToAdd name={'Атака групп'}/>
                <TagToAdd name={'Угол/сторона/центр'}/>
            </div>
            <div className={"tags-container tactics"}>
                <div className={"mini-title"}>Тактика:</div>
                <TagToAdd name={'Виды соединений'}/>
                <TagToAdd name={'Количество дамэ'}/>
                <TagToAdd name={'Оценка стоимости хода (есэ)'}/>
                <TagToAdd name={'Ёсэ тесудзи'}/>
                <TagToAdd name={'Сенте-готэ (есэ)'}/>
                <TagToAdd name={'Вторжение в угол'}/>
                <TagToAdd name={'Вторжение в зону'}/>
                <TagToAdd name={'Тесудзи'}/>
                <TagToAdd name={'Гашение зоны'}/>
            </div>
            </div>
        </div>
        </div>
        </div>);
}

function ParseComment(comment) {
    if (!comment) return ["", []]
    let parts = comment.split(TAGS_SEP)
    console.log(parts)
    if (parts.length <= 1) {
        parts.push("")
        return [parts[0], []]
    }
    if (parts[1].trim().length === 0) return [parts[0], []]
    return [parts[0], parts[1].trim().split('\n')]
}

function ParseTags(comment) {
    const [main_comment, tags] = ParseComment(comment)
    return tags
}

export default SGFEditor;