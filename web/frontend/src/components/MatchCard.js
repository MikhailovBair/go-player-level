import React from "react";
import "../styles/MatchCard.css"

function MatchCard({match}) {
    const white = match.player_white
    const black = match.player_black
    const date = match.date
    const desc = match.result
    const title = black + ' vs. ' + white
    const link = 'https://tromp.github.io/img/go/legal19.gif'
    return (
        <div className={'match_card'}>
            <img src={link} alt={'go'}/>
            <div className={'info_tile'}>
                <div className={'match_title'}>{title}</div>
                <div className={'match_date'}>{date}</div>
                <div className={'match_desc'}>{desc}</div>
            </div>
        </div>
    )
}

export default MatchCard;