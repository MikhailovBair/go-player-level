import React from "react";
import "../styles/ProfileLine.css"

function ProfileLine(props) {
    const { name, value } = props;
    return (
        <div className='profile-line'>
            <div className='profile-name'>
                {name}
            </div>
            <div className='profile-value'>
                {value}
            </div>
        </div>
    )
}

export default ProfileLine;
