import React, {useState} from "react";
import "../styles/PlayerSetting.css"
import Switch from "react-switch";
import axios from "axios";
import {backend} from "../config";

function RegularSetting({name, placeholder, val, onChange, prop}) {
    return (
        <div className={'regular_setting'}>
            <div className={'name'}>{name}</div>
            <div className={'field'}>
                <input type='text' id={name} name={prop} placeholder={placeholder} value={val} onChange={onChange}/>
            </div>
        </div>
    )
}

function CalibratingFileSetting({name, button_text}) {
    const [file, setFile] = useState(null);
    const [clarificationVisible, changeClarificationVisible] = useState(false);
    function submitGame(e, clr) {
        e.preventDefault()
        if (file === null) {
            return;
        }
        let form_data = new FormData();
        form_data.append('file', file);
        form_data.append('color', clr);
        changeClarificationVisible(true);
        axios.post(backend + 'api/player/submit_calibrating/', form_data,
        {
            timeout: 1000*1000,
            headers: {
                "Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`,
                'content-type': 'multipart/form-data'
            }
        }
        ).then(response => {
            alert("Рейтинг обновлен")
            window.location.reload()
        }).catch(error => {
            alert("Некорретный запрос")
            console.log(error)
        })
    }
    return (
        <div className={'file_setting'}>
            <div className={'name'}>{name}</div>
            <div className={'second_part'}>
                <input className={'file-pick'}
                       onChange={(e)=>{
                           setFile(e.target.files[0])
                       }}
                       name="game-file" type="file"/>
                <div className={'button'} onClick={e=>submitGame(e,"B")}>Я за чёрных</div>
                <div className={'button'} onClick={e=>submitGame(e,"W")}>Я за белых</div>
                {
                    (clarificationVisible) ?
                    <div className={"clarification"}>Через ~1 минуту рейтинг обновится сам. Не сохраняйте изменения во
                        время обработки партии</div> : <div/>
                }
            </div>
        </div>
    )
}

function PasswordSetting({name, onChange, prop}) {
    return (
        <div className={'regular_setting'}>
            <div className={'name'}>{name}</div>
            <div className={'field'}>
                <input type='password' id={name} name={prop} onChange={onChange}/>
            </div>
        </div>
    )
}

export {RegularSetting, CalibratingFileSetting, PasswordSetting}
