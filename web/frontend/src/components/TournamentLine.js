import React from "react";
import { Link } from "react-router-dom";
import "../styles/TournamentLine.css"

function TournamentLine(props) {
    const { title, id } = props;
    return (
        <Link to={'/judge/tournament/' + id} className='link'>
            <div className='tournament-line'>
                {title}
            </div>
        </Link>
    )
}

export default TournamentLine;