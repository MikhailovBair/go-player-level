import React from "react";
import "../styles/JudgeLogin.css"
import axios from "axios";
import {backend} from "../config";
import {useNavigate} from "react-router-dom";
import { Link } from "react-router-dom";

function JudgeLogin() {
    const navigate = useNavigate()
    const [login, setLogin] = React.useState();
    const [password, setPassword] = React.useState();

    const handleClick = (e) => {
        e.preventDefault()

        axios.post(backend + 'api/token/', {
            username: login,
            password: password
        }).then(response => {
            if (response.status !== 200) {
                console.log(response.data)
                return
            }

            navigate("/judge/tournaments-list", {replace: true});
            window.location.reload();

            window.localStorage.setItem('ACCESS', response.data.access);
            window.localStorage.setItem('REFRESH', response.data.refresh);
        }).catch(error => {
            if (error.response.statusText === 'Unauthorized') {
                // updateErrorMessage('Неправильный логин или пароль')
                alert('Неправильный логин или пароль')
            }
        })
    }

    return (
        <div className='login-wrapper'>
            <div className='title'>
                Авторизация
            </div>
            <form className='reg-form'>
                <div className='data-input'>
                    <label>Логин</label>
                    <input
                        name='login'
                        type='text'
                        value={login}
                        onChange={(event) => {
                            setLogin(event.target.value);
                        }}
                    />
                </div>

                
               
                <div className='data-input'>
                    <label>Пароль</label>
                    <input
                        name='password'
                        type='password'
                        value={password}
                        onChange={(event) => {
                            setPassword(event.target.value);
                        }}
                    />
                </div>
                
                <Link to={'/'} className='link'>
                    <div className='back-button'>Назад</div>
                </Link>
                <div className='reg-button' onClick={handleClick}>Вход</div>
            </form>
        </div>
    )
}

export default JudgeLogin;
