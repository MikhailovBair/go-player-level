import React from "react";
import "../styles/JudgeTournamentsList.css"
import HeaderJudge from "./HeaderJudge.js";
import TournamentLine from "./TournamentLine";
import {backend} from "../config";
import axios from "axios";
//import { useHistory } from 'react-router';


function JudgeTournamentsList(props) {
    //const {tournaments} = props;
    //const history = useHistory();
    const [title, setTitle] = React.useState();
    const [tournaments, setTournaments] = React.useState(null);
    const [file, setFile] = React.useState();
    // api/tournaments/
    axios.get(backend + "api/judge/tournament-list/",
    {
        headers: {
            "Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`
        }
    }
        ).then(response => {
        if (response.data.id !== null) {
            if (!tournaments) {
                setTournaments(response.data['tournaments']);
            }
            console.log(tournaments);
            //window.localStorage.setItem('_tournaments', JSON.stringify(tournaments));
        }
    }).catch(error => {})
    /*{tournaments.map((tournament)=>(
        <TournamentLine title={tournament.title}/>
    ))} */



    const handleClick = (e) => {
        e.preventDefault()
        //let file = document.getElementById('input').files[0]
        //console.log(file)
        let form_data = new FormData();
        form_data.append('title', title);
        form_data.append('file', file)
        //  api/tournaments/
        axios.post(backend + 'api/judge/submit_csv/', form_data,
        {
            headers: {
                "Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`,
                'content-type': 'multipart/form-data'
            }   
        }
            ).then(response => {
            if (response.status !== 200) {
                //console.log(response.data)
                //console.log(response)
                window.location.reload();
                return
            }
            console.log(response)
            
        }).catch(error => {
            console.log(error)
        })
    }
    return (
       <div className='judge-tournaments-list'>
            <HeaderJudge/>

            <div className='wrapper'>
                <div className='title'>
                    Турниры
                </div>
                <div className='tournaments-list'>
                    {tournaments ? tournaments.map((tournament)=>(
                        <TournamentLine title={tournament.title} id={tournament.id}/>
                    )) :
                    <div/>}
                </div>


                <form className='create-tournament-form'>
                    <div className='data-input'>
                        <label>Название турнира</label>
                        <input
                            name='tournament-title'
                            type='text'
                            value={title}
                            onChange={(event) => {
                                setTitle(event.target.value);
                            }}
                        />
                    </div>
                    
                    <div className='download'>
                        <input
                            name='download-file'
                            type='file'
                            onChange={(event) => {
                                setFile(event.target.files[0]);
                            }}
                        />
                    </div>

                    <button
                            type='submit'
                            className='tournament-create-button'
                            onClick={handleClick}
                        >
                            Создать турнир
                    </button>
                </form>

            </div>
        </div>
    )
}

export default JudgeTournamentsList;