import React from "react";
import "../styles/JudgeTournament.css"
import HeaderJudge from "./HeaderJudge.js";
import {useParams} from "react-router-dom";
import CustomCsvToTable from "./CustomCsvToTable";
import {backend} from "../config";
import axios from "axios";
import InfoLine from "./InfoLine";


function insertId(data, colIdx, rowIdx, id) {
    data[rowIdx][colIdx] = id
}

function updateTournament(url, data, colIdx, rowIdx, id) {
    const rows = data.split('\n');
    const table = rows.map(row => row.split(','));
    insertId(table, colIdx, rowIdx, id);
    const upd_rows = table.map(row => row.join(','));
    const upd_data = upd_rows.join('\n');

    fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'text/plain'
        },
        data: upd_data}).then(
            (response) => {
              if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                  response.status);
                console.log(response);
                console.log(upd_data);
                return;
              }});
}


function get_name(data) {
    const cell = data.split(',')[0]
    return cell.split(':')[1]
}

function get_start(data){
    const row = data.split('\n')[3]
    const cell = row.split(',')[0]
    return cell.split(':')[1]
}

function get_finish(data){
    const row = data.split('\n')[4]
    const cell = row.split(',')[0]
    return cell.split(':')[1]
}

function get_place(data){
    const row = data.split('\n')[1]
    const cell = row.split(',')[0]
    return cell.split(':')[1]
}

function JudgeTournament(props) {
    const params = useParams();
    const {tournamentId} = params;
    //const [tournament} = props;
    const [tour, setTour] = React.useState();
    const [csv_data, setCsvData] = React.useState();
    const [id, setId] = React.useState();
    const [files, setFiles] = React.useState();
    const [games, setGames] = React.useState();
    const [name, setName] = React.useState();
    const [start, setStart] = React.useState();
    const [finish, setFinish] = React.useState();
    const [place, setPlace] = React.useState();

    const [tournament, setTournament] = React.useState();
    const [title, setTitle] = React.useState();

    // const tournaments = JSON.parse(window.localStorage.getItem('_tournaments'));
    // let new_tournament = null;
    // let title = null;
    // tournaments.map(function(tournament) {
    //     if (tournament['id'] == tournamentId) {
    //         new_tournament = tournament['file'];
    //         title = tournament['title'];
    //         //break;
    //     }
    //   });

    axios.get(backend + "api/tournaments/"+tournamentId+'/',
    {
        headers: {
            "Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`
        }
    }
    ).then(function(response) {
        console.log(response.data)
        setTitle(response.data['title'])
        setTournament(response.data['file'].replace("http://127.0.0.1:8000/", ""))
        fetch(backend + tournament).then(
            function(response) {
              if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                  response.status);
                return;
              }
        
              // Examine the text in the response
              response.text().then(function(data) {
                    setCsvData(data);
                    console.log(data)
                    setName(get_name(data));
                    setStart(get_start(data));
                    setFinish(get_finish(data));
                    setPlace(get_place(data));
                   //console.log(csv_data);
              });
            }
          )
          .catch(function(err) {
            console.log('Fetch Error :-S', err);
          });
    }
    );
  const handleClick = (e) => {
    e.preventDefault()
    //let file = document.getElementById('input').files[0]
    //console.log(file)
    for (let i = 0; i < files.length; i++) {
        let form_data = new FormData();
        form_data.append('tour', tour);
        form_data.append('file', files[i]);
        form_data.append('tournament', tournamentId);
        //console.log(tournamentId);
        //axios.post(backend + 'api/judge-games/', form_data,
        axios.post(backend + 'api/new-judge-game/', form_data,
        {
            headers: {
                'content-type': 'multipart/form-data',
                "Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`
            }   
        }
            ).then(response => {
            if (response.status !== 200) {
                //console.log(response.data)
                setId(response.data.id)
                
                return
            }
            console.log(response)
            
        }).then(()=> {
            //console.log(id)
        }).catch(error => {
            console.log(error)
        })
        if (i === files.length - 1) {
            window.location.reload();
        }
    }
}
//?tournament="+tournamentId
axios.get(backend + "api/judge-games/?tournament="+tournamentId).then(response => {
    if (response.data.id !== null) {
        if (!games) {
            setGames(response.data);
        }
        //console.log(games);
        
    }
}).catch(error => {})


    return (
       <div className='judge-tournament'>
            <HeaderJudge/>

            <div className='wrapper'>
                <div className='title'>
                    {/* {title} */}
                    {(name)? name : 'Rfr'}
                </div>
                <InfoLine name='Начало турнира' value={(start)? start: 'none'}/>
                <InfoLine name='Окончание турнира' value={(finish)? finish: 'none'}/>
                <InfoLine name='Место проведения' value={(place)? place: 'none'}/>

                <div className='tournament-table'>
                    {/* {csv_data ?
                        <CsvToHtmlTable
                            data={csv_data}
                            csvDelimiter=","
                            tableClassName="table-striped table-hover"
                        />:
                        <div/>
                    } */}
                    {(csv_data && games)?
                      <CustomCsvToTable 
                        data={csv_data}
                        del = ','
                        id={id}
                        games={games}
                        />
                        :
                        <div/>
                    }
                
                </div>

                <form className='load-games-form'>
                    <div className='title'>
                        Данные о партии
                    </div>

                    {/* <div className='data-input'>
                        <label>Игрок</label>
                        <input
                            name='player'
                            type='text'
                            value={player}
                            onChange={(event) => {
                                setPlayer(event.target.value);
                            }}
                        />
                    </div> */}

                    <div className='data-input'>
                        <label>Номер тура</label>
                        <input
                            name='tour'
                            type='text'
                            value={tour}
                            onChange={(event) => {
                                setTour(event.target.value);
                            }}
                        />
                    </div>
                    
                    <div className='download'>
                        <input
                            name='download-file'
                            type='file'
                            multiple
                            onChange={(event) => {
                                setFiles(event.target.files);
                            }}
                        />
                    </div>

                    <button
                            type='submit'
                            className='tournament-create-button'
                            onClick={handleClick}
                        >
                            Загрузить партию
                    </button>
                </form>

            </div>
        </div>
    )
}

export default JudgeTournament;
