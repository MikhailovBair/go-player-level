import React, {useState} from "react";
import {Routes, Route, BrowserRouter} from 'react-router-dom';
import "../styles/App.css"
import MainPage from "./MainPage";
import HeaderPlayer from "./HeaderPlayer";
import MatchesPage from "./MatchesPage";
import PlayerProfile from "./PlayerProfile";
import JudgeRegistration from "./JudgeRegistraion";
import PlayerRegistration from "./PlayerRegistration";
import JudgeLogin from "./JudgeLogin";
import PlayerLogin from "./PlayerLogin";
import JudgeProfile from "./JudgeProfile";
import JudgeGame from "./JudgeGame";
import JudgeTournament from "./JudgeTournament";
import JudgeTournamentsList from "./JudgeTournamentsList";
import SGFEditor from "./SGFEditor";
import {backend} from "../config";
import axios from "axios";
import TagStats from "./TagStats";


const matchlist = [
    {'black_nick': 'SanKingTim', 'white_nick': 'MikeTayson', 'date': '21.03.2022', 'desc': 'ЧМ по боксу. 88-55'},
    {'black_nick': 'KekMekPek', 'white_nick': 'Vyatsheslav', 'date': '22.22.2022', 'desc': 'Кубок 315. 44-44.5'},
    {'black_nick': 'SanKingTim', 'white_nick': 'MikeTayson', 'date': '21.03.2022', 'desc': 'ЧМ по боксу. 88-55'},
    {'black_nick': 'SanKingTim', 'white_nick': 'MikeTayson', 'date': '21.03.2022', 'desc': 'ЧМ по боксу. 88-55'},
]

const judgeStat = {
  login : 'igoryan', name : 'Игорь Петрович', surname : 'Сидоров', email : 'igorsid@gmail.com'
}

const judgeGame = {
  desc : 'Москва 2022 1-й тур',
  'black' : {name : 'Солодов Алексей', sgf_rating : '17k', model_rating : '17k'},
  'white' : {name : 'Сурков Вячеслав', sgf_rating : '17k', model_rating : '16k'},
}

const tournaments_list = [
  { 
    title: 'Moscow 2022'
  },
  {
    title: 'Zelenograd 2022'
  }
]

function App() {
    const [name, setName] = useState(null);
    const [refreshed, setRefreshed] = useState(null);

    axios.post(backend + "api/token/refresh/", {
        refresh: window.localStorage.getItem('REFRESH')
    }).then(response => {
        window.localStorage.setItem('ACCESS', response.data.access);
        setRefreshed(1);
        return axios.get(backend + "api/current_user/", {
        "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`}
        })
    }).then(response => {
        if (response.data.id !== null)
            setName(response.data.first_name + " " + response.data.last_name)
    }).catch(error => {})

  return (
      <BrowserRouter>
        <Routes>
            <Route path='/' element={<MainPage/>}/>
            <Route path='/header' element={<HeaderPlayer username={name}/>}/>
            <Route path='/player/list' element={<MatchesPage username={name}/>}/>
            <Route path='/player/profile' element={<PlayerProfile username={name}/>}/>
            <Route path='/player/registration' element={<PlayerRegistration/>} />
            <Route path='/player/login' element={<PlayerLogin/>} />
            <Route path='/judge/registration' element={<JudgeRegistration/>} />
            <Route path='/judge/login' element={<JudgeLogin/>} />
            <Route path='/judge/profile' element={<JudgeProfile stat={judgeStat}/>} />
            <Route path='/judge/game/:gameId' element={<JudgeGame stat={judgeGame}/>} />
            <Route path='/judge/tournaments-list' element={<JudgeTournamentsList tournaments={tournaments_list}/>} />
            <Route path='/player/sgf/:game_id' element={<SGFEditor username={name}/>}/>
            <Route path='/judge/tournament/:tournamentId' element={<JudgeTournament tournament={tournaments_list[0]}/>} />
            <Route path='/player/tags_stats' element={<TagStats username={name}/>}/>
        </Routes>
      </BrowserRouter>
  );
}

export default App;
