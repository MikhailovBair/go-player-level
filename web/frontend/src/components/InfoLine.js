import React from "react";
import "../styles/InfoLine.css"

function InfoLine(props) {
    const { name, value } = props;
    return (
        <div className='info-line'>
            <div className='info-name'>
                {name}
            </div>
            <div className='info-value'>
                {value}
            </div>
        </div>
    )
}

export default InfoLine;
