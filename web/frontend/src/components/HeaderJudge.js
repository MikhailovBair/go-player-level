import React from "react";
import "../styles/HeaderJudge.css";
import { Link } from "react-router-dom";

function HeaderJudge(props) {
    const { login, name, surname, email } = props;
    return (
        <div className='judge-header'>
            <Link to={'/judge/tournaments-list'} className='link'>
                <div className='menu-button'>
                    Турниры
                </div>
            </Link>
            <div className='menu-button'>
                Профиль
            </div>
            <Link to={'/'} className='link'>
                <div className='exit-button'>
                    Выход
                </div>
            </Link>
        </div>
    )
}

export default HeaderJudge;
