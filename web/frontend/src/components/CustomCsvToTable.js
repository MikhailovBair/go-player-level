import React from "react";
import { Link } from "react-router-dom";
import "../styles/CustomCsvToTable.css"


function CustomCsvToTable(props) {
    const {data, del, id, games} = props;
    const rows = data.split('\n');
    if (!rows || rows.length === 0) {
        return [];
    }

    const table = rows.map(row => row.split(del));
    //console.log(table)

    const TableHeader = (row) => {
        if (row && row.map) {
          return (
            <thead>
              <tr>
                {
                  row.map((column, i) => (
                    <th
                      key={`header-${i}`}
                    >
                      {column}
                    </th>
                  ))
                }
              </tr>
            </thead>
          );
        }
      };


      const get_tour = function(data, colIdx) {
        return data[6][colIdx];
      }

      const get_name = function(data, rowIdx) {
        //console.log(data)
        //console.log(rowIdx)
        //console.log(data[rowIdx])
        return data[rowIdx][3];
      }

      const get_id = function(data, colIdx, rowIdx, games) {
        for (let i = 0; i < games.length; ++i) {
          //console.log(rowIdx)
          let cur_name = get_name(data, rowIdx);
          let cur_tour = get_tour(data, colIdx);
          
          if ((cur_tour == games[i]['tour']) && 
          (cur_name.toLowerCase() == games[i]['black_name'].toLowerCase() ||
           cur_name.toLowerCase() == games[i]['white_name'].toLowerCase())) {
            return {
              is_model: false,
              id: games[i]['id'],
              file: games[i]['file']
            }
            //games[i]['id'];
          } else {
            if ((cur_tour == games[i]['tour']+'_model') && 
              (cur_name.toLowerCase() == games[i]['black_name'].toLowerCase() ||
              cur_name.toLowerCase() == games[i]['white_name'].toLowerCase())) {
                let rating = (cur_name.toLowerCase() == games[i]['black_name'].toLowerCase())? 
                              games[i]['b_model_rating'] : games[i]['w_model_rating'];
                return {
                  is_model: true,
                  rating: rating,
                  id: games[i]['id']
                }
           }
          }
        }
        return {
          is_model: false,
          id: null
        };
      }

      const writeColumn = function(column, id, rowIdx, colIdx) {
        if (rowIdx == 100000 & colIdx == 8) {
          if (id) {
            return id;
          } else {
            return 'AAA';
          }
        } else {
          return column;
        }
      }

      const rating_to_num = function(rating) {
        let dan = rating.substr(rating.length - 1)
        let number = rating.slice(0, -1);
        if (dan == 'd') {
          return Number(number) - 1;
        } else {
          return -Number(number);
        }
      } 

      const getStyle = function(rating, pred_rating) {
          if (pred_rating == 'no-rating') {
            return 'custom-column';
          } else {
            let delta = Math.abs(rating_to_num(rating) - rating_to_num(pred_rating));
            if (delta > 3) {
              console.log(delta)
              return 'red-custom-column';
            }

            return 'custom-column';
          }
      }

      const getRating = function(table, rowIdx) {
        return table[rowIdx][5];
      }

      const getLink = function(column, rowIdx, colIdx) {
        //console.log(rowIdx);
        const result = get_id(table, colIdx, rowIdx+7, games);
        let id = result['id'];
        let is_model = result['is_model'];
        //console.log(game)
  
        if (id) {
          if (is_model) {
            let rating = result['rating'];
            console.log(rating)
            let true_rating = getRating(table, rowIdx+7);
            let style = getStyle(true_rating, rating);
            return (
              <Link to={'/judge/game/'+id} className='link'>
              
                <td
                  className={style}
                >
                  {rating/* {writeColumn(column, id, rowIdx, colIdx)} */}
                </td>
                        
              </Link>
            )
          } else {
            let file = result['file'];
            console.log(file)
            return (
              <td
                  className='custom-column'
                >
                  <a href={file} target="_blank" download>
                    {writeColumn(column, id, rowIdx, colIdx)}
                  </a>
              </td>
            )
          }
        } else {
          return (
            <td
                className='custom-column'
              >
                {writeColumn(column, id, rowIdx, colIdx)}
            </td>
          )
        }
      }

      const TableBody = (rows) => {
        if (rows && rows.map) {
          return (
            <tbody>
              {
                rows.map((row, rowIdx) => (
                  <tr className='custom-row' >
                    {
                      row.map((column, colIdx) => (
                        // <td
                        //   className='custom-column'
                        // >
                        //   {writeColumn(column, id, rowIdx, colIdx)}
                        // </td>
                        
                        getLink(column, rowIdx, colIdx)
                        
                      ))
                    }
                  </tr>
                ))
              }
            </tbody>
          );
        }
      };

    return (
       <div>
            <table className='custom-table'>
                {TableHeader(table[6])}
                {TableBody(table.slice(7))}
            </table>
        </div>
    )
}

export default CustomCsvToTable;