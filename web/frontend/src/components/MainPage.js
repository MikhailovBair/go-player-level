import React from "react";
import "../styles/MainPage.css"
import SignTile from "./SignTile";
import {Link} from "react-router-dom";

function MainPage() {
    const img_url = 'https://media.wired.com/photos/59325dedaef9a462de98235c/master/w_2500,h_1667,c_limit/Go-04.jpg'

    const description = 'Го — логическая настольная игра с глубоким стратегическим содержанием, возникшая в Древнем Китае, по разным оценкам, от 2 до 5 тысяч лет назад. До XIX века культивировалась исключительно в Восточной Азии, в XX веке распространилась по всему миру. По общему числу игроков — одна из самых распространённых настольных игр в мире. Входит в число пяти базовых дисциплин Всемирных интеллектуальных игр.'

    return (
    <div className={'main_page'}>
        <p className={'title'}>GO Helper</p>
        <div className={'left_tile'}>
            <img className={'goimg'} src={img_url} alt={'Игра Го'}/>
            <div className={'description'}>{description}</div>
        </div>
        <div className={'right_part'}>
            <div className={'judge'}><SignTile title={'Судья'}/></div>
            <div className={'player'}><SignTile title={'Игрок'}/></div>
        </div>
    </div>
    )
}

export default MainPage;