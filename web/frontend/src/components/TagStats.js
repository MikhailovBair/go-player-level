import React, {useEffect, useState} from "react";
import "../styles/TagStats.css";
import HeaderPlayer from "./HeaderPlayer";
import axios from "axios";
import {backend} from "../config";
import {Link} from "react-router-dom";

function TagStats({username}) {
    const [stats, changeStats] = useState([]);
    const [pairs, changePairs] = useState([])
    useEffect(() => {
        axios.get(backend+'api/player/tag_stats/',
            {
            "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
        }).then(response => {
            changeStats(response.data.stats)
            console.log(response.data)
        })
        axios.get(backend+'api/player/all_tag_games/',
            {
            "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
        }).then(response => {
            changePairs(response.data.result)
            console.log(response.data)
        })

    }, [])

    return <div>
        <HeaderPlayer username={username}/>
        <div className={"stats-page"}>
            <div className={"title"}>Статистика по тегам</div>
            <div className={"table"}>
            {
                stats.map(item => {
                    return <div className={"row"}>
                        <div className={"tag"}>{item[0]}</div>
                        <div className={"number"}>| {item[1][0]} ошибок</div>
                        <div className={"number"}>| {item[1][1]} партий</div>
                    </div>
                })
            }
            </div>
            <div className={"title"}>Партии с тегами</div>
            <div className={"table"}>
            {
                pairs.map(item => {
                    return <div className={"row"}>
                        <div className={"tag"}>{item.tag}</div>
                        <div className={"number"}>| Ход {item.move}</div>
                        <div className={"number"}>{"| "}
                            <Link to={"/player/sgf/"+item.game_id}>{item.title}</Link>
                        </div>
                    </div>
                })
            }
            </div>
        </div>
    </div>
}

export default TagStats;