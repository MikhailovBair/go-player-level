import React from "react";
import "../styles/PlayerRegistration.css"
import axios from "axios";
import {backend} from "../config";
import { useNavigate } from "react-router-dom";

function PlayerRegistration() {
    const navigate = useNavigate()
    const [login, setLogin] = React.useState();
    const [name, setName] = React.useState();
    const [surname, setSurname] = React.useState();
    const [email, setEmail] = React.useState();
    const [password, setPassword] = React.useState();

    const handleClick = (e) => {
        e.preventDefault()

        axios.post(backend + 'api/player/register/', {
            username: login,
            password: password,
            first_name: name,
            last_name: surname,
            email: email
        }).then(response => {
            if (response.status !== 201) {
                console.log(response.data)
                return
            }

            navigate("/", {replace: true});
            window.location.reload();
        }).catch(error => {
            if (error.response.data === 'User with the same username already exists') {
                alert('Пользователь с таким именем уже существует')
            }
        })
    }

    return (
        <div className='reg-player-wrapper'>
            <div className='title'>
                Регистрация
            </div>
            <form className='reg-form'>
                <div className='reg-data-input'>
                    <label>Логин</label>
                    <input
                        name='login'
                        type='text'
                        value={login}
                        onChange={(event) => {
                            setLogin(event.target.value);
                        }}
                    />
                </div>

                <div className='reg-data-input'>
                    <label>Имя</label>
                    <input
                        name='name'
                        type='text'
                        value={name}
                        onChange={(event) => {
                            setName(event.target.value);
                        }}
                    />
                </div>

                <div className='reg-data-input'>
                    <label>Фамилия</label>
                    <input
                        name='surname'
                        type='text'
                        value={surname}
                        onChange={(event) => {
                            setSurname(event.target.value);
                        }}
                    />
                </div>

                <div className='reg-data-input'>
                    <label>Почта</label>
                    <input
                        name='email'
                        type='text'
                        value={email}
                        onChange={(event) => {
                            setEmail(event.target.value);
                        }}
                    />
                </div>

                <div className='reg-data-input'>
                    <label>Пароль</label>
                    <input
                        name='password'
                        type='password'
                        value={password}
                        onChange={(event) => {
                            setPassword(event.target.value);
                        }}
                    />
                </div>

                <div className='back-button'>
                        Назад
                </div>

                <div className='reg-button' onClick={handleClick}>
                        Зарегистрироваться
                </div>
            </form>
        </div>
    )
}

export default PlayerRegistration;
