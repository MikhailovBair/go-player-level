import React from "react";
import "../styles/JudgeGame.css"
import "./HeaderJudge.js"
import HeaderJudge from "./HeaderJudge.js";
import JudgeGameLine from "./JudgeGameLine";
import {backend} from "../config";
import axios from "axios";
import { useParams, Link } from 'react-router-dom';

function JudgeGame(props) {
    // const {desc} = props;
    // const { login, name, surname, email } = stat;
    const desc = 'Турнир'
    const [stat, setStat] = React.useState();
    const params = useParams();
    const {gameId} = params;
    axios.get(backend + "api/judge-games/"+gameId+'/').then(response => {
        if (response.data.id !== null) {
            if (!stat) {
                console.log(response.data);
                setStat(response.data);
            }
        
        }
    });
    return (
       <div>
            <HeaderJudge/>

            <div className='wrapper'>
                <div className='title'>
                    {desc}
                </div>
                {stat ?
                <JudgeGameLine name='Игрок' b_value={stat['black_name']} w_value={stat['white_name']}/>
                :
                <div/>}
                <JudgeGameLine name='Цвет' b_value='черный' w_value='белый'/>
                {stat ?
                <JudgeGameLine name='Рейтинг sgf' b_value={stat['black_rating']} w_value={stat['white_rating']}/>
                :
                <div/>}
                {stat ?
                <JudgeGameLine name='Рейтинг по программе' b_value={stat['b_model_rating']} w_value={stat['w_model_rating']}/>
                :
                <div/>}

                <div className='judge-game-back-button'>
                    {stat ?
                        <Link to={'/judge/tournament/'+stat['tournament']} className='link'>
                            Назад
                        </Link>:
                    <div/>
                    }
                </div>
            </div>
        </div>
    )
}

export default JudgeGame;
