import React, {useEffect, useState} from "react";
import HeaderPlayer from "./HeaderPlayer";
import {RegularSetting, CalibratingFileSetting, PasswordSetting} from "./PlayerSetting"
import axios from "axios";
import {backend} from "../config";

import "../styles/PlayerProfile.css"
import {useNavigate} from "react-router-dom";

const img_url = 'https://ichef.bbci.co.uk/news/640/cpsprodpb/475B/production/_98776281_gettyimages-521697453.jpg'

function PlayerProfile({username, userinfo}) {
    const navigate = useNavigate()

    const initialUserInfo = ({
        username: '',
        email: '',
        first_name: '',
        last_name: '',
        password: '',
    })
    const initialPlayerInfo = ({
        nickname1: '',
        nickname2: '',
        nickname3: '',
        rating: ''
    })
    const [userInfo, updateUserInfo] = useState(initialUserInfo)
    let [newUserInfo, updateNewUserInfo] = useState(initialUserInfo)
    const [playerInfo, updatePlayerInfo] = useState(initialPlayerInfo)
    let [newPlayerInfo, updateNewPlayerInfo] = useState(initialPlayerInfo)
    const [photoUrl, updatePhotoUrl] = useState("")
    const [selectedFile, setSelectedFile] = useState(null);


    useEffect(() => {
        const user_info_req = axios.get(backend + "api/current_user/",
            {
                "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`}
            }
        )
        const add_info_req = axios.get(backend + "api/player/info/",
            {
                "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`}
            }
        )

        axios.all(
            [user_info_req, add_info_req]
        ).then(
            axios.spread((...responses) => {
            if (responses[0].data.id !== null) {
                updateUserInfo({
                    username: responses[0].data.username,
                    email: responses[0].data.email,
                    first_name: responses[0].data.first_name,
                    last_name: responses[0].data.last_name,
                    password: ""
                })
                updatePlayerInfo({
                    nickname1: responses[1].data.nickname1,
                    nickname2: responses[1].data.nickname2,
                    nickname3: responses[1].data.nickname3,
                    rating: responses[1].data.rating,
                })
            }
        })).catch(e => {
            navigate('/', {replace: true})
            window.location.reload();
        })
    }, [])

    useEffect(() => {
        axios.get(backend + "api/photo/",
            {
                "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`}
            }
        ).then(response => {
            if (response.data.id !== null) {
                updatePhotoUrl(response.data['img_url'])
            }
        }).catch(e => {
            navigate('/', {replace: true})
            window.location.reload();
        })
    }, [])

    useEffect(() => {
        axios.get(backend + "api/player/info/",
            {
                "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`}
            }
        ).then(response => {
            if (response.data.id !== null) {
                updatePlayerInfo(response.data)
            }
        }).catch(e => {
            navigate('/', {replace: true})
            window.location.reload();
        })
    }, [])

    const handleChange = (e, update, what) => {
        console.log(e.target.name)
        // if (e.target.name === 'password_repeat') {
        //     update_password_repeat(e.target.value.trim())
        //     return
        // }
        update({
            ...what,
            [e.target.name]: e.target.value.trim()
        })
        console.log(what)
    }

    const saveChanges = (e) => {
        console.log('kek')
        if (newUserInfo['username'] === '') newUserInfo['username'] = userInfo['username']
        if (newUserInfo['first_name'] === '') newUserInfo['first_name'] = userInfo['first_name']
        if (newUserInfo['last_name'] === '') newUserInfo['last_name'] = userInfo['last_name']
        if (newUserInfo['email'] === '') newUserInfo['email'] = userInfo['email']
        if (newPlayerInfo['nickname1'] === '') newPlayerInfo['nickname1'] = playerInfo['nickname1']
        if (newPlayerInfo['nickname2'] === '') newPlayerInfo['nickname2'] = playerInfo['nickname2']
        if (newPlayerInfo['nickname3'] === '') newPlayerInfo['nickname3'] = playerInfo['nickname3']
        if (newPlayerInfo['rating'] === '') newPlayerInfo['rating'] = playerInfo['rating']

        axios.post(backend+'api/change_creds/', newUserInfo, {
            "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
        }).then(response => {
            console.log(newPlayerInfo)
            return axios.post(backend+'api/player/change_info/', newPlayerInfo, {
                "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
            })
        }).then(response => {
            window.location.reload();
        }).catch(error => {
            if (error.response.data === 'User with the same username already exists') {
                alert('Пользователь с таким именем уже существует')
            }
        })
    }

    const photoClick = (e) => {
        let form_data = new FormData();
        form_data.append('image', selectedFile);

        console.log(form_data)
        axios.post(backend+"api/change_photo/", form_data, {
            headers: {
                'content-type': 'multipart/form-data',
                "Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`
            }
            })
            .then(res => {
              console.log(res.data);
              window.location.reload()
            })
            .catch(err => console.log(err))
    }

    return (
        <div>
            <HeaderPlayer username={username}/>
            <div className={'player_profile'}>
                <div className={'settings_container'}>
                    <RegularSetting name={'Логин'} placeholder={userInfo.username}
                                    val={newUserInfo.username}
                                    onChange={(e) => handleChange(e, updateNewUserInfo, newUserInfo)}
                                    prop={'username'}/>
                    <RegularSetting name={'Фамилия'} placeholder={userInfo.last_name}
                                    val={newUserInfo.last_name}
                                    onChange={(e) => handleChange(e, updateNewUserInfo, newUserInfo)}
                                    prop={'last_name'}/>
                    <RegularSetting name={'Имя'} placeholder={userInfo.first_name}
                                    val={newUserInfo.first_name}
                                    onChange={(e) => handleChange(e, updateNewUserInfo, newUserInfo)}
                                    prop={'first_name'}/>
                    <RegularSetting name={'Email'} placeholder={userInfo.email}
                                    val={newUserInfo.email}
                                    onChange={(e) => handleChange(e, updateNewUserInfo, newUserInfo)}
                                    prop={'email'}/>
                    <RegularSetting name={'Ник 1'} placeholder={playerInfo.nickname1}
                                    val={newPlayerInfo.nickname1}
                                    onChange={(e) => handleChange(e, updateNewPlayerInfo, newPlayerInfo)}
                                    prop={'nickname1'}/>
                    <RegularSetting name={'Ник 2'} placeholder={playerInfo.nickname2}
                                    val={newPlayerInfo.nickname2}
                                    onChange={(e) => handleChange(e, updateNewPlayerInfo, newPlayerInfo)}
                                    prop={'nickname2'}/>
                    <RegularSetting name={'Рейтинг'} placeholder={playerInfo.rating}
                                    val={newPlayerInfo.rating}
                                    onChange={(e) => handleChange(e, updateNewPlayerInfo, newPlayerInfo)}
                                    prop={'rating'}/>
                    <CalibratingFileSetting name={'Калибровочная партия'} button_text={'Загрузить файл'}/>
                    <PasswordSetting name={'Новый пароль'}
                                    onChange={(e) => handleChange(e, updateNewUserInfo, newUserInfo)}
                                    prop={'password'}/>
                    <div className='save_button' onClick={saveChanges}>Сохранить</div>
                </div>
                <div className={'photo_setting'}>
                    <img className={'photo'} src={backend.slice(0, -1) + photoUrl} alt={'Фото'}/>
                         <input
                      type="file"
                      id="file"
                      name="file"
                      placeholder="Загрузите изображение"
                      onChange={(e) => setSelectedFile(e.target.files[0])}
                    />
                    <div className={'change_button'} onClick={photoClick}>Изменить фото</div>
                </div>
            </div>
        </div>
    )
}

export default PlayerProfile;