import React from 'react'
import "../styles/HeaderPlayer.css"
import {Link} from "react-router-dom";

function HeaderPlayer({username}) {
    return (
        <div className={'header_player'}>
            <div className={'title'}>ГО.Игрок</div>
            <div className={'options'}>
                <div className={'option'}><Link to={'/player/list'}><a href='/'>Партии</a></Link></div>
                <div className={'option'}><Link to={'/player/tags_stats'}><a href='/'>Статистика</a></Link></div>
                <div className={'option'}><Link to={'/player/profile'}><a href='/'>{username}</a></Link></div>
                <div className={'option'}><a href='/'>Выйти</a></div>
            </div>
        </div>
    )
}

export default HeaderPlayer;