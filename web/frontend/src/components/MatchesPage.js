import React, {useEffect, useState} from "react";
import "../styles/MatchesPage.css"
import HeaderPlayer from "./HeaderPlayer";
import MatchCard from "./MatchCard";
import axios from "axios";
import {backend} from "../config";
import {Link} from "react-router-dom";

function MatchesPage({username}) {

    const [current_list, update_list] = useState([])
    const [match_list, update_match_list] = useState([])
    const [file, setFile] = React.useState();
    const init_query = ""
    const [query, update_query] = useState(init_query)
    const [filterMyMatches, updateFilterMyMatches] = useState(false);
    const [loaded, set_loaded] = useState(false)
    const [myNickNames, set_myNicknames] = useState([])
    useEffect( () => {
            axios.get(backend+'api/player/info/',
                {
                "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
            }).then(response => {
                set_myNicknames([response.data.nickname1.toLowerCase(), response.data.nickname2.toLowerCase()])
                return axios.get(backend + 'api/player/game_list/',
            {
                    "headers": {"Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`},
                }
            )}).then(response => {
                update_match_list(response.data['games'])
                update_list(match_list)
                console.log(match_list)
                set_loaded(true)
            }).catch(error => {
                // alert("Ошибка")
            })
        }, [loaded]
    )


    const handleChangeSearch = (e) => {
        if (e.target.name === 'search') {
            update_query(e.target.value.trim())
        }
    }

    const filterMatches = (only_my) => {
        console.log(myNickNames)
        let current_list = match_list
        current_list = current_list.filter(
            (item) => {
                const all = item.player_black + item.player_white + item.date + item.result
                return all.toLowerCase().indexOf(query.toLowerCase()) !== -1
            }
        )
        if (only_my) {
            current_list = current_list.filter(
            (item) => {
                    return myNickNames.indexOf(item.player_black.toLowerCase()) !== -1 ||
                        myNickNames.indexOf(item.player_white.toLowerCase()) !== -1
                }
            )
        }
        update_list(current_list)
    }

    const handleClickSearch = (e) => {
        filterMatches(filterMyMatches)
    }

    const submitFile = (e) => {
        e.preventDefault()
        let form_data = new FormData();
        form_data.append('file', file);
        axios.post(backend + 'api/player/submit_sgf/', form_data,
        {
            headers: {
                "Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`,
                'content-type': 'multipart/form-data'
            }
        }
        ).then(response => {
            window.location.reload();
            return axios.post(backend + 'api/player/analyze/', response.data,
                {
                timeout: 1000*1000,
                headers: {
                    "Authorization": `Bearer ${window.localStorage.getItem('ACCESS')}`
                }
            })
        }).catch(error => {
            alert(error)
        })
    }

    return (
        <div>
            <HeaderPlayer username={username}/>
            <div className={'matches_page'}>
                <div className={'title'}>Партии</div>
                <div className={'search_item'}>
                    <div className={'tick-label'}>Только мои партии</div>
                    <div className={'tick'}>
                        <input type='checkbox' id={'filter_my_matches'}
                               onChange={e=>{
                                   filterMatches(!filterMyMatches)
                                   updateFilterMyMatches(!filterMyMatches)
                               }}/>
                    </div>
                    <div className={'search_string'}>
                        <input type='text' id={'search'} name={'search'}
                               className='search_string' placeholder={'Поиск'}
                               onChange={handleChangeSearch}/>
                    </div>
                    <div className={'button'} onClick={handleClickSearch}>Найти</div>
                </div>
                <div className={'main_part'}>
                    <div>Загрузить новую игру</div>
                    <form className='submit-new-game'>
                        <div className='download'>
                            <input
                                name='download-file'
                                type='file'
                                onChange={(event) => {
                                    setFile(event.target.files[0]);
                                }}
                            />
                        </div>
                    </form>
                    <div className={'button'} onClick={submitFile}>Загрузить</div>

                    <div className={'tiles_list'}>
                        {current_list.map((item)=><Link to={'/player/sgf/'+item.id} key={item.id}><MatchCard match={item} /></Link>)}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MatchesPage;