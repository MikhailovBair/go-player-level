import React from "react";
import "../styles/JudgeGameLine.css"

function JudgeGameLine(props) {
    const { name, b_value, w_value } = props;
    return (
        <div className='judge-game-line'>
            <div className='b-value'>
                {b_value}
            </div>
            <div className='stat-name'>
                {name}
            </div>
            <div className='w-value'>
                {w_value}
            </div>
        </div>
    )
}

export default JudgeGameLine;