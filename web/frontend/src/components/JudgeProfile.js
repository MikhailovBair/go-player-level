import React from "react";
import "../styles/JudgeProfile.css"
import "./HeaderJudge.js"
import HeaderJudge from "./HeaderJudge.js";
import ProfileLine from "./ProfileLine";

function JudgeProfile(props) {
    const {stat} = props;
    const { login, name, surname, email } = stat;
    return (
       <div>
            <HeaderJudge/>

            <div className='wrapper'>
                <div className='title'>
                    Профиль
                </div>
                <ProfileLine name='Логин' value={login}/>
                <ProfileLine name='Имя' value={name}/>
                <ProfileLine name='Фамилия' value={surname}/>
                <ProfileLine name='Почта' value={email}/>
            </div>
        </div>
    )
}

export default JudgeProfile;
