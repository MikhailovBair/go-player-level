import React from "react";
import "../styles/SignTile.css"
import {Link} from "react-router-dom";

function SignTile({title}) {
    const part = (title === 'Игрок') ? 'player' : 'judge'
    return (
        <div className={'sign_tile'}>
            <div className={'title'}>{title}</div>
            <hr className={'line'}/>
            <div className={'links'}>

                <Link to={'/'+part+'/login'}><div className={'link'}><a href={'/'}>Вход</a></div></Link>
                <Link to={'/'+part+'/registration'}><div className={'link'}><a href={'/'}>Регистрация</a></div></Link>
            </div>
        </div>
    );
}

export default SignTile;
