from django.contrib import admin

from .models import *

admin.site.register(JudgeAdditionalInfo)
admin.site.register(PlayerAdditionalInfo)
admin.site.register(ProfilePhoto)
admin.site.register(Game)
