import asyncio
import json
import traceback
from collections import Counter

import requests
from asgiref.sync import sync_to_async
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions

from django.contrib.auth.models import User
from rest_framework import viewsets, mixins
from rest_framework_simplejwt.tokens import RefreshToken

from rest_framework.parsers import MultiPartParser, FormParser

from .serializers import PlayerAdditionalInfoSerializer, JudgeAdditionalInfoSerializer, UserSerializer, \
    ProfilePhotoSerializer
from .sgf_utils import get_tags_from_bytes
from ..models import JudgeAdditionalInfo, PlayerAdditionalInfo, ProfilePhoto, Game
from sgfmill import sgf

import httpx


class CurrentUser(APIView):
    def get(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        serializer = UserSerializer(request.user)
        return Response(serializer.data)


class Registration(APIView):

    def post(self, request):
        try:
            user = User.objects.create(**request.data)
            user.set_password(request.data['password'])
            user.save()
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='User with the same username already exists')
        return Response(status=status.HTTP_201_CREATED)


class ChangeCreds(APIView):

    def post(self, request):
        user = request.user
        if user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        try:
            if user.username != request.data['username']:
                user.username = request.data['username']
            user.email = request.data['email']
            user.first_name = request.data['first_name']
            user.last_name = request.data['last_name']
            if len(request.data['password']) > 0:
                user.set_password(request.data['password'])

            user.save()
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='User with the same username already exists')

        return Response(status=status.HTTP_201_CREATED)


class LogoutView(APIView):

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            print(e)
            return Response(status=status.HTTP_400_BAD_REQUEST)


class PhotoView(APIView):

    def get(self, request):
        if request.user.is_anonymous:
            return Response({
                'img_url': '/media/resources/blank.png'
            })
        infos = ProfilePhoto.objects.filter(user_id=request.user.id)
        if len(infos) == 0:
            return Response({
                'img_url': '/media/resources/blank.png'
            })
        return Response({
            'img_url': infos[0].photo.url
        })


class ChangePhotoView(ListAPIView):
    queryset = ProfilePhoto.objects.all()
    serializer_class = ProfilePhotoSerializer

    def post(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        ProfilePhoto.objects.filter(user_id=request.user.id).delete()

        info = ProfilePhoto.objects.create(user_id=request.user.id, photo=request.data['image'])
        return Response(status=status.HTTP_201_CREATED)


class PlayerAdditionalInfoView(APIView):
    def get(self, request):

        empty = {
            'nickname1': '',
            'nickname2': '',
            'nickname3': '',
            'rating': ''
        }

        if request.user.is_anonymous:
            return Response(empty)
        infos = PlayerAdditionalInfo.objects.filter(user_id=request.user.id)
        if len(infos) == 0:
            return Response(empty)
        return Response({
            'nickname1': infos[0].nickname1,
            'nickname2': infos[0].nickname2,
            'nickname3': infos[0].nickname3,
            'rating': infos[0].rating
        })


class ChangePlayerAdditionalInfoView(ListAPIView):
    queryset = PlayerAdditionalInfo.objects.all()
    serializer_class = PlayerAdditionalInfoSerializer

    def post(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        PlayerAdditionalInfo.objects.filter(user_id=request.user.id).delete()
        info = PlayerAdditionalInfo.objects.create(user_id=request.user.id,
                                                   nickname1=request.data['nickname1'],
                                                   nickname2=request.data['nickname2'],
                                                   nickname3=request.data['nickname3'],
                                                   rating=request.data['rating'])
        return Response(status=status.HTTP_201_CREATED)


class GetGame(APIView):
    def get(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        games = Game.objects.filter(user_id=request.user.id, id=request.query_params['game_id'])
        if len(games) == 0:
            return Response(status=status.HTTP_403_FORBIDDEN)
        text = games[0].sgf.read()
        return Response({
            'sgf': text.decode(errors='ignore')
        })


class SaveGame(APIView):
    def post(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        games = Game.objects.filter(user_id=request.user.id, id=request.data['game_id'])
        if len(games) == 0:
            return Response(status=status.HTTP_403_FORBIDDEN)
        game = games[0]
        game.sgf.open('wb')
        game.sgf.write(request.data['sgf'].encode('utf8'))
        game.sgf.close()
        return Response(status=status.HTTP_201_CREATED)


class GetGamesList(APIView):
    def get(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        games = Game.objects.filter(user_id=request.user.id)
        matches = []
        for game in games:
            parsed = sgf.Sgf_game.from_bytes(game.sgf.read()).get_root()
            try:
                player_black = parsed.get_raw('PB').decode(errors='ignore')
            except KeyError:
                player_black = "Black"
            try:
                player_white = parsed.get_raw('PW').decode(errors='ignore')
            except KeyError:
                player_white = "White"
            try:
                date = parsed.get('DT')
            except KeyError:
                date = "No date"
            try:
                result = parsed.get('RE')
            except KeyError:
                result = "No result"

            except Exception as e:
                print(traceback.format_exc())
                continue
            to_add = {
                'id': game.id,
                'player_black': player_black,
                'player_white': player_white,
                'date': date,
                'result': result
            }
            matches.append(to_add)

        return Response({
            'games': matches
        })


class AddNewGamePlayer(APIView):
    def post(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        game = Game.objects.create(user_id=request.user.id, sgf=request.data['file'])
        game.save()

        return Response(status=status.HTTP_201_CREATED, data={"game_id": game.id})


class AnalyzeGame(APIView):
    def post(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        games = Game.objects.filter(user_id=request.user.id, id=request.data['game_id'])
        if len(games) == 0:
            return Response(status=status.HTTP_403_FORBIDDEN)
        game = games[0]

        url = 'https://gogetblunders-kkbft5v6xa-lm.a.run.app/'

        r = requests.post(url,
                          files={"file": game.sgf.open('rb')},
                          timeout=1000)
        if r.status_code != 200:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        mistakes_json = r.json()
        print(mistakes_json)
        # mistakes_json = {'blunders_black': [5, 1, 3],
        #                  'blunders_white': [10, 20],
        #                  'mistakes_black': [7],
        #                  'mistakes_white': [2, 4]}
        result = {'Black': [], 'White': []}
        for move in mistakes_json['blunders_black']:
            result['Black'].append({'move': move, 'type': 'blunder'})
        for move in mistakes_json['blunders_white']:
            result['White'].append({'move': move, 'type': 'blunder'})
        for move in mistakes_json['mistakes_black']:
            result['Black'].append({'move': move, 'type': 'mistake'})
        for move in mistakes_json['mistakes_white']:
            result['White'].append({'move': move, 'type': 'mistake'})
        result['Black'].sort(key=lambda x: x['move'])
        result['White'].sort(key=lambda x: x['move'])

        game.loaded = True
        game.mistakes_json = json.dumps(result)
        game.save()
        return Response(status=status.HTTP_201_CREATED)


class TagStats(APIView):
    def get(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        games = Game.objects.filter(user_id=request.user.id)
        counter = Counter()
        counter_unique = Counter()
        for game in games:
            game_bytes = game.sgf.read()
            game_tags = [tag[1] for tag in get_tags_from_bytes(game_bytes)]
            unique_tags = list(set(game_tags))
            for tag in game_tags:
                counter[tag] += 1
            for tag in unique_tags:
                counter_unique[tag] += 1
        result = {}
        for tag in counter.keys():
            result[tag] = (counter[tag], counter_unique[tag])

        for tag in counter_unique.keys():
            result[tag] = (counter[tag], counter_unique[tag])
        return Response({'stats': list(result.items())})


class AllTagGamePairs(APIView):
    def get(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        games = Game.objects.filter(user_id=request.user.id)
        result = []
        for game in games:
            game_bytes = game.sgf.read()
            parsed = sgf.Sgf_game.from_bytes(game_bytes).get_root()
            try:
                title = parsed.get_raw("PB").decode(errors='ignore') + " vs " + parsed.get_raw("PW").decode(errors='ignore')
            except:
                title = f"Game {game.id}"
            result.extend([
                {"game_id": game.id, "move": tag[0], "tag": tag[1], "title": title} for tag in get_tags_from_bytes(game_bytes)
            ])
        result.sort(key=(lambda x: x['tag']))
        return Response({
            "result": result
        })


class SubmitCalibratingSgf(APIView):
    def post(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        print(request.data['file'])
        url = 'https://getgoprediction-kkbft5v6xa-lm.a.run.app/'
        r = requests.post(url,
                          files={"file": request.data['file'].open('rb')},
                          timeout=1000)
        if r.status_code != 200:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        data = r.json()

        info = PlayerAdditionalInfo.objects.filter(user_id=request.user.id)
        if len(info) == 0:
            player = PlayerAdditionalInfo.objects.create(user_id=request.user.id)
        else:
            player = info[0]
        player.rating = data['W_rank'] if request.data['color'] == 'W' else data['B_rank']
        player.save()
        return Response(data)


class GetGameMistakes(APIView):
    def get(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        games = Game.objects.filter(user_id=request.user.id, id=request.query_params['game_id'])
        if len(games) == 0:
            return Response(status=status.HTTP_403_FORBIDDEN)
        game = games[0]
        if not game.loaded:
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(json.loads(game.mistakes_json))
