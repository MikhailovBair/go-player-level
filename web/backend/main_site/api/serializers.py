from django.contrib.auth.models import User
from ..models import PlayerAdditionalInfo, JudgeAdditionalInfo, ProfilePhoto, Game
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class ProfilePhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfilePhoto
        fields = '__all__'


class PlayerAdditionalInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlayerAdditionalInfo
        fields = '__all__'


class JudgeAdditionalInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = JudgeAdditionalInfo
        fields = '__all__'
