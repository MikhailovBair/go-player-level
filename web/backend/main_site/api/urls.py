from django.contrib import admin
from django.urls import path, include
from .views import *
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

urlpatterns = [
    path('player/register/', Registration.as_view(), name='player reg'),
    path('current_user/', CurrentUser.as_view(), name='current'),
    path('change_creds/', ChangeCreds.as_view(), name='change general user credentials'),
    path('photo/', PhotoView.as_view(), name='photo'),
    path('change_photo/', ChangePhotoView.as_view(), name='change photo'),
    path('player/info/', PlayerAdditionalInfoView.as_view(), name='player info'),
    path('player/change_info/', ChangePlayerAdditionalInfoView.as_view(), name='change info'),
    path('player/get_game/', GetGame.as_view(), name='get game'),
    path('player/save_game/', SaveGame.as_view(), name='save game'),
    path('player/game_list/', GetGamesList.as_view(), name='game_list'),
    path('player/submit_sgf/', AddNewGamePlayer.as_view(), name='new game'),
    path('player/analyze/', AnalyzeGame.as_view(), name='analyze'),
    path('player/tag_stats/', TagStats.as_view(), name='tag stats'),
    path('player/all_tag_games/', AllTagGamePairs.as_view(), name='all pairs tag-game'),
    path('player/submit_calibrating/', SubmitCalibratingSgf.as_view(), name='submit calibrating game'),
    path('player/get_mistakes/', GetGameMistakes.as_view(), name='get mistakes in game')
]
