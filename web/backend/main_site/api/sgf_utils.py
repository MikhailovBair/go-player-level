from sgfmill import sgf

TAGS_SEP = "===TAGS==="


def get_tags_from_comment(comment):
    parts = comment.split(TAGS_SEP)
    if len(parts) <= 1:
        return []
    tags = parts[1].split('\n')
    tags = list(map(lambda x: x.strip(' \n'), tags))
    tags = list(filter(lambda x: x != '', tags))
    return tags


def get_tags_from_bytes(game_bytes):
    try:
        result = []
        parsed = sgf.Sgf_game.from_bytes(game_bytes)
        for i, node in enumerate(parsed.get_main_sequence()):

            if node.has_property("C"):
                result.extend([
                    (i + 1, tag) for tag in
                    get_tags_from_comment(node.get("C"))
                ])
        return result
    except:
        return []
