from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class ProfilePhoto(models.Model):
    photo = models.ImageField(default='/media/resources/blank.png', verbose_name='photo')
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class JudgeAdditionalInfo(models.Model):
    # TODO
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class PlayerAdditionalInfo(models.Model):
    nickname1 = models.CharField(max_length=50, default='')
    nickname2 = models.CharField(max_length=50, default='')
    nickname3 = models.CharField(max_length=50, default='')
    rating = models.CharField(max_length=10, default='')
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Game(models.Model):
    sgf = models.FileField(upload_to='SGFBase')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    loaded = models.BooleanField(default=False)
    mistakes_json = models.TextField(null=True)
