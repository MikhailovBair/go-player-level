from django.shortcuts import render
from django.views.generic import ListView

from .serializers import TournamentsSerializer
from .models import Tournament
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status


class TournamentsListView(viewsets.ModelViewSet):
    serializer_class = TournamentsSerializer
    queryset = Tournament.objects.all()

class GetTournamentList(APIView):
    def get(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        filt = Tournament.objects.filter(user_id=request.user.id)
        tournaments = []
        for tournament in filt:
            tournaments.append({
                'id': tournament.id,
                'title': tournament.title,
            })
        print(tournaments)
        return Response({
            'tournaments': tournaments
        })



class AddNewTournament(APIView):
    def post(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        tournament = Tournament.objects.create(user_id=request.user.id, file=request.data['file'],
                                                title=request.data['title'])
        tournament.save()
        return Response(status=status.HTTP_201_CREATED)