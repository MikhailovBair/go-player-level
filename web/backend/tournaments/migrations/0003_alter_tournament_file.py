# Generated by Django 3.2.9 on 2022-05-14 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournaments', '0002_tournament_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tournament',
            name='file',
            field=models.FileField(upload_to='tournaments'),
        ),
    ]
