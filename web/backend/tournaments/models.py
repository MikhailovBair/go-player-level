from django.db import models
from django.core.files.base import ContentFile
from django.core.files import File
from django.contrib.auth.models import User



class Tournament(models.Model):
    title = models.CharField(max_length=100)
    file = models.FileField(upload_to='tournaments')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        
        if not self.pk and self.file.name.split('.')[0][-5:] != 'fixed':
           
            print('Hello')
            print(self.file.name)
            print(self.pk)
            data = ''
            f = self.file.open()
            data = f.read().decode('utf-8')
            #data = f.read().decode('')
            print(data)
            data = data.split('\n')

            header = data[6].split(',')
            ind = []
            for i in range(len(header)):
                if header[i].isnumeric():
                    ind.append(i)
            
            new_data = data[0:6]
            print(ind)
            for i, row in enumerate(data[6:-1]):
                new_row = row.split(',')
                for el in ind[::-1]:
                    if i == 0:
                        tour = new_row[el]
                        #new_row.insert(el + 1, tour+'_link')
                        new_row.insert(el + 1, tour+'_model')
                    else:
                        #new_row.insert(el + 1, '')
                        new_row.insert(el + 1, '')
                new_row.pop(len(new_row) - 2)
                print(','.join(new_row))
                new_data.append(','.join(new_row))
            print(new_data)
            new_data = '\n'.join(new_data)
            print(new_data)
            f1 = ContentFile(new_data)
            new_name = self.file.name.split('.')
            new_name[0] = new_name[0] + 'fixed'
            new_name = '.'.join(new_name)
            #self.file = File(f1)
            self.file.save(new_name, f1)
            return
            #self.file.seek(0)
        super(Tournament, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.title}'
