from django.db import models
import sys
sys.path.append('..')
from tournaments.models import Tournament
from django.db.models.signals import pre_save
from django.dispatch import receiver

# Create your models here.

class JudgeGame(models.Model):
    tour = models.IntegerField(default=0)
    black_name = models.CharField(max_length=100, default='неопознанный шакал')
    white_name = models.CharField(max_length=100, default='неопознанный шакал')
    black_rating = models.CharField(max_length=10, default='no-rating')
    white_rating = models.CharField(max_length=10, default='no-rating')
    b_model_rating = models.CharField(max_length=10, default='no-rating')
    w_model_rating = models.CharField(max_length=10, default='no-rating')
    file = models.FileField(upload_to='judge-games')
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE, default=0)

    


    def __str__(self):
        return f'{self.tour}-{self.black_name}-{self.white_name}'

@receiver(pre_save, sender=JudgeGame)
def _mymodel_pre_save(sender, instance, **kwargs):

    if not instance.pk:
        
        # print('Hello')
        # print(instance.file.name)
        # print(self.pk)
        
        f = instance.file.open()
        data = f.read().decode('utf-8')

        ind = data.find('PW[')
        ind += 3
        ind2 = data.find(']', ind)
        instance.white_name = data[ind:ind2]

        ind = data.find('PB[')
        ind += 3
        ind2 = data.find(']', ind)
        instance.black_name = data[ind:ind2]

        ind = data.find('WR[')
        ind += 3
        ind2 = data.find(']', ind)
        instance.white_rating = data[ind:ind2]

        ind = data.find('BR[')
        ind += 3
        ind2 = data.find(']', ind)
        instance.black_rating = data[ind:ind2]
        #instance.save()