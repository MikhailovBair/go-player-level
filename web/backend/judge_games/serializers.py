from rest_framework import serializers
from .models import JudgeGame

class JudgeGamesSerializer(serializers.ModelSerializer):
    class Meta:
        model = JudgeGame
        fields = ['id', 'tour', 'black_name', 'white_name', 'black_rating',
        'white_rating', 'b_model_rating', 'w_model_rating', 'file', 'tournament']