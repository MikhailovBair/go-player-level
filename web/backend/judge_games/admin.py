from django.contrib import admin

# Register your models here.
from . models import JudgeGame

admin.site.register(JudgeGame)