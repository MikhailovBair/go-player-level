from django.apps import AppConfig


class JudgeGamesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'judge_games'
