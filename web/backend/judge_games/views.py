from time import sleep
from django.shortcuts import render
from django.views.generic import ListView



from .serializers import JudgeGamesSerializer
from .models import JudgeGame
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django_filters.rest_framework import DjangoFilterBackend, BaseInFilter, NumberFilter, FilterSet
from tournaments.models import Tournament
import requests


class NumberInFilter(BaseInFilter, NumberFilter):
    pass

class JudgeGameFilter(FilterSet):
   id = NumberInFilter(field_name='id', lookup_expr='in')
   class Meta:
       fields = ['tournament']
       model = JudgeGame


class JudgeGamesListView(viewsets.ModelViewSet):
    serializer_class = JudgeGamesSerializer
    queryset = JudgeGame.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_class = JudgeGameFilter


class AddNewJudgeGame(APIView):
    def post(self, request):
        if request.user.is_anonymous:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        
        print(request.data)
        print('+++++++++++++++++++++++++++++++++++++++')
        tournament = Tournament.objects.filter(id=int(request.data['tournament']))[0]
        game = JudgeGame.objects.create(file=request.data['file'],
                                         tour=request.data['tour'], tournament=tournament)
        
        url = 'https://getgoprediction-kkbft5v6xa-lm.a.run.app/'
        r = requests.post(url,
                          files={"file": request.data['file'].open('rb')},
                          timeout=1000)
        if r.status_code != 200:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        data = r.json()
        while data.get('error', 0) != 0:
            sleep(5)
            r = requests.post(url,
                          files={"file": request.data['file'].open('rb')},
                          timeout=1000)
            if r.status_code != 200:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            data = r.json()
        print('**************************************')
        print(r)
        print(data)
        print('**************************************')
        game.w_model_rating = data['W_rank'] 
        game.b_model_rating = data['B_rank']

        game.save()
        return Response(status=status.HTTP_201_CREATED)