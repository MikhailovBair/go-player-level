"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework import routers
from tournaments.views import TournamentsListView, AddNewTournament, GetTournamentList
from judge_games.views import JudgeGamesListView, AddNewJudgeGame
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = routers.DefaultRouter()
router.register('tournaments', TournamentsListView)
router.register('judge-games', JudgeGamesListView)

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('api/', include('main_site.api.urls'), name='api'),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/judge/submit_csv/', AddNewTournament.as_view()),
    path('api/new-judge-game/', AddNewJudgeGame.as_view()),
    path('api/judge/tournament-list/', GetTournamentList.as_view()),
    path('api/', include(router.urls)),
    re_path(r'^', include('main_site.urls'), name='frontend')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns[-1], urlpatterns[-2] = urlpatterns[-2], urlpatterns[-1]
