import pytest
import selenium.webdriver.chrome.webdriver
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from time import sleep

driver: selenium.webdriver.chrome.webdriver.WebDriver = None
BASE = 'http://localhost:3000/'


def setup():
    global driver
    driver = webdriver.Chrome('./chromedrivers/chromedriver.exe')


def teardown():
    driver.quit()


def player_login(username, password):
    driver.get(BASE)
    button_login = driver.find_element(By.XPATH, '//*[@id="root"]/div/div[2]/div[2]/div/div[2]/a[1]')
    button_login.click()
    username_string = driver.find_element(By.XPATH, '//*[@id="root"]/div/form/div[1]/input')
    password_string = driver.find_element(By.XPATH, '//*[@id="root"]/div/form/div[2]/input')
    username_string.send_keys(username)
    password_string.send_keys(password)
    submit_button = driver.find_element(By.XPATH, '//*[@id="root"]/div/form/div[4]')
    submit_button.click()


def test_player_login():
    '''
    Сценарий: пользователь жмет на ссылку войти, вводит логин и пароль
    Ожидается: произойдет переход на страницу профиля, в шапке написано имя пользователя
    '''
    username = 'test'
    password = 'test'
    player_login(username, password)
    sleep(0.5)
    header_name = driver.find_element(By.XPATH, '//*[@id="root"]/div/div[1]/div[2]/div[2]/a/a')
    assert header_name.text == 'Тест Тестыч'


def test_player_login_wrong_creds():
    '''
    Сценарий: пользователь жмет на ссылку войти, вводит неправильные логин и/или пароль
    Ожидается: появляется alert с сообщением о неверных данных
    '''
    username = 'test'
    password = 'test1'
    player_login(username, password)
    WebDriverWait(driver, 3).until(ec.alert_is_present(),
                                    'Alert for wrong creds')

    alert = driver.switch_to.alert
    assert alert.text == 'Неправильный логин или пароль'
    alert.accept()


def test_player_logout():
    '''
    Сценарий: пользователь заходит на сайт и жмет на ссылку "Выйти"
    Ожидается: переход на главную страницу
    '''
    username = 'test'
    password = 'test'
    player_login(username, password)
    sleep(0.5)
    logout_button = driver.find_element(By.XPATH, '//*[@id="root"]/div/div[1]/div[2]/div[3]')
    logout_button.click()
    assert driver.current_url == BASE


def test_change_creds():
    '''
    Сценарий: пользователь заходит в свой профиль и меняет некоторые поля профиля
    Ожидается: данные профиля обновляются
    '''
    username = 'test'
    password = 'test'
    new_nick = 'chiller'
    player_login(username, password)
    sleep(0.5)
    nick2_string = driver.find_element(By.XPATH, '/html/body/div/div/div[2]/div[1]/div[6]/div[2]/input')
    nick2_string.send_keys(new_nick)
    submit_button = driver.find_element(By.XPATH, '//*[@id="root"]/div/div[2]/div[1]/div[10]')
    submit_button.click()
    sleep(0.5)
    nick2_string = driver.find_element(By.XPATH, '/html/body/div/div/div[2]/div[1]/div[6]/div[2]/input')
    assert nick2_string.get_property('placeholder') == new_nick


def test_matches_filtering():
    '''
    Сценарий: пользователь заходит в свой профиль,
    переходит на страницу со списком игр, далее с помощью строки фильтрации ищет игры по названию
    Ожидается: до фильтрации отображается 4 игры, после - 2.
    '''
    username = 'test'
    password = 'test'
    player_login(username, password)
    sleep(0.5)
    matches_link = driver.find_element(By.XPATH, '//*[@id="root"]/div/div[1]/div[2]/div[1]/a')
    matches_link.click()
    sleep(0.5)
    cards = driver.find_elements(By.CLASS_NAME, 'match_card')
    assert len(cards) == 4

    filter_string = driver.find_element(By.XPATH, '/html/body/div/div/div[2]/div[2]/div[3]/input')
    filter_string.send_keys('2d')
    filter_button = driver.find_element(By.XPATH, '//*[@id="root"]/div/div[2]/div[2]/div[4]')
    filter_button.click()
    cards = driver.find_elements(By.CLASS_NAME, 'match_card')
    assert len(cards) == 3


def test_tags_management():
    '''
    Сценарий: пользователь заходит в свой профиль,
    переходит на страницу со списком игр, переходит на страницу с анализом, добавляет и удаляет тег
    Ожидается: тег после добавления должен появиться, а после удаления исчезнуть
    '''
    
    username = 'test'
    password = 'test'
    player_login(username, password)
    sleep(0.5)
    matches_link = driver.find_element(By.XPATH, '//*[@id="root"]/div/div[1]/div[2]/div[1]/a')
    matches_link.click()
    sleep(0.5)
    cards = driver.find_elements(By.CLASS_NAME, 'match_card')
    cards[0].click()
    sleep(0.3)
    tag_to_add = driver.find_element(By.CSS_SELECTOR, '.tags-container > .tag')
    tag_text = tag_to_add.text
    tag_to_add.click()
    sleep(0.3)
    tag_to_del = driver.find_element(By.CSS_SELECTOR, '.current-tags .tag')
    assert tag_to_del.text == tag_text
    tag_to_del.click()
    sleep(0.3)
    added_tags = driver.find_elements(By.CSS_SELECTOR, '.current-tags .tag')
    assert len(added_tags) == 0
