require 'spec_helper'

describe package('nodejs'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe package('npm'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe command('npm view axios version'), :if => os[:family] == 'ubuntu' do
  its(:stdout) {should be >= '0.26.1'}
end

describe command('npm view react version'), :if => os[:family] == 'ubuntu' do
  its(:stdout) {should be >= '17.0.2'}
end

describe command('npm view react-csv-to-table version'), :if => os[:family] == 'ubuntu' do
  its(:stdout) {should be >= '0.0.1'}
end

describe command('npm view react-dom version'), :if => os[:family] == 'ubuntu' do
  its(:stdout) {should be >= '17.0.2'}
end

describe command('npm view react-router-dom version'), :if => os[:family] == 'ubuntu' do
  its(:stdout) {should be >= '6.3.0'}
end

describe command('npm view react-scripts version'), :if => os[:family] == 'ubuntu' do
  its(:stdout) {should be >= '5.0.0'}
end

describe command('npm view web-vitals version'), :if => os[:family] == 'ubuntu' do
  its(:stdout) {should be >= '2.1.4'}
end

describe package('python3'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe command('python3 --version'), :if => os[:family] == 'ubuntu' do
  its(:stdout) {should be >= 'Python 3.8'}
end

describe command('python3 -m pip freeze'), :if => os[:family] == 'ubuntu' do
  # its(:stdout) {should contain 'asgiref==3.4.1'}
  # its(:stdout) {should contain 'Django==3.2.9'}
  # its(:stdout) {should contain 'django-cors-headers==3.11.0'}
  # its(:stdout) {should contain 'djangorestframework==3.12.4'}
  # its(:stdout) {should contain 'djangorestframework-simplejwt==5.1.0'}

  its(:stdout) {should contain 'asgiref==3.4.1'}
  its(:stdout) {should contain 'Django==3.2.9'}
  its(:stdout) {should contain 'django-cors-headers==3.11.0'}
  its(:stdout) {should contain 'djangorestframework==3.12.4'}
  its(:stdout) {should contain 'djangorestframework-simplejwt==5.1.0'}
  its(:stdout) {should contain 'PyJWT==2.3.0'}
  its(:stdout) {should contain 'pytz==2021.1'}
  its(:stdout) {should contain 'sqlparse==0.4.2'}

end